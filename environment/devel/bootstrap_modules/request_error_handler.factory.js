(function() {
  'use strict';
  
  /**
   * Monica Modena
   * The service DcyRequestErrorHandler manages Contains the data about the error on the HTTP request.  
   * It views the error message.   
   */
  function DcyRequestErrorHandler($q, $log) {
	  
	  var service = {};
	  
	  function responseError(rejection) {
		  
		  //switch rejection.status
		  var errorContainer = rejection.data, // data.data necessario per come � incapsulato il data nella response in caso di errore. 
		    errorData = (errorContainer && errorContainer.data) ? errorContainer.data : '', 
		  	errorStatus = rejection.status,
		  	errorHeaders = rejection.headers,
		  	errorConfig = rejection.config,
		  	errorStatusText = rejection.statusText,
		  	errorDPR = (errorData) ? errorData.error : undefined,
		  	errorCode = (errorDPR) ? errorDPR.errorCode : errorStatus,
		  	errorMsg = (errorDPR && errorDPR.errorMsg) ? errorDPR.errorMsg : errorStatusText,
		  	errorDetails = (errorDPR) ? errorDPR.errorDetails : undefined,
		  	message = errorMsg;
		  
		  if (errorCode) {
			  message = errorCode + ': ' + message;
		  }
		  
		  $log.error('#DcyRequestErrorHandler: ' + message);
		  $log.error(errorDPR);

		  return $q.reject(rejection);
	  }
	  
	  service = {
			  responseError: responseError
	  };

	  return service;
	 
  }
  
  DcyRequestErrorHandler.$inject = ['$q',  '$log'];

  angular.module('dcyApp.factories').factory('dcyRequestErrorHandler', DcyRequestErrorHandler);

}());
