(function() {
    'use strict';

    function DCDataTableController($scope, $timeout, $q) {
        $scope.DECISYON.target.registerDataConnector(function(requestor) {
            var deferer = $q.defer();
            $timeout(function() {
                deferer.resolve([{
                    "index": 1,
                    "name": "Liz Grimes",
                    "country": "UK",
                    "date": "Mon Jul 06 1987"
                }, {
                    "index": 2,
                    "name": "Frazier Lara",
                    "country": "USA",
                    "date": "Tue May 24 1988"
                }, {
                    "index": 3,
                    "name": "Jmaes Joseph",
                    "country": "Italy",
                    "date": "Wed May 25 1988"
                }, {
                    "index": 4,
                    "name": "Mark Karl",
                    "country": "Spain",
                    "date": "Wed May 27 1989"
                }, {
                    "index": 5,
                    "name": "Steve Thomption",
                    "country": "France",
                    "date": "Wed July 30 1991"
                }]);

            }, 3000);
            return {
                data: deferer.promise
            };
        });
    }

    DCDataTableController.$inject = ['$scope', '$timeout', '$q'];
    DECISYON.ng.register.controller('dcDataTableCtrl', DCDataTableController);

}());